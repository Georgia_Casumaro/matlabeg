function [mean, standardDev] = computeStdDev(inputMatrix)

    % compute the mean of all the elements 
    mean = computerAverage(inputMatrix);
    
    % Compute the numerator 
    [m,n] = size(inputMatrix)
    sum = 0; 
    for i = 1:m
        for j = 1:n
            sum = sum + (inputMatrix(i,j) - mean)^2;
        end 
    end 
    % compute the standard deviation 
    standardDev = sqrt(sum/(m*n -1)); 
end 