function averageVal = computerAverage(inputMatrix)
    % returns the average of a matix input 
    
    % determine the size of the matrix 
    [m, n] = size(inputMatrix);
    % sum of all the elements 
    sum = 0;
    for i = 1:m
        for j = 1:n
            sum = sum + inputMatrix(i,j);
        end 
    end 
    % compute the average value
    averageVal = sum/(i*j);
end 